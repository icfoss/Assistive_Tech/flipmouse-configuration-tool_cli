# FlipMouse Configuration Tool_CLI

>The command line tool for configuring the FlipMouse. This project was initially maintained as a sub project under the FlipMouse project.

The [FlipMouse](https://gitlab.com/icfoss/Assistive_Tech/flipmouse)
requires a tool for configuring different paramters 
so as to customise it for a particular user. This 
command line tool is **developed** for **UNIX based
systems** *(other platforms not tested)*, so that 
you can plug in your FlipMouse and configure the 
parameters on th go with minimal knowledge of its 
working.

 ## Index
 - [Introduction](https://gitlab.com/icfoss/Assistive_Tech/flipmouse-configuration-tool_cli#inroduction)
 - [How to run the script](https://gitlab.com/icfoss/Assistive_Tech/flipmouse-configuration-tool_cli#how-to-run-the-script)
    - [Installing the dependencies](https://gitlab.com/icfoss/Assistive_Tech/flipmouse-configuration-tool_cli#commandlinepy)
 - [How to plug in your FlipMouse](https://gitlab.com/icfoss/Assistive_Tech/flipmouse-configuration-tool_cli#how-to-plug-in-your-flipmouse)
    - [Port Permissions](https://gitlab.com/icfoss/Assistive_Tech/flipmouse-configuration-tool_cli/#port-pemissions)
 - [Connection Status and Firmware Version](https://gitlab.com/icfoss/Assistive_Tech/flipmouse-configuration-tool_cli#connection-status-and-firmware-version)
    - [Commands in the cli_tool](https://gitlab.com/icfoss/Assistive_Tech/flipmouse-configuration-tool_cli#commands-in-the-cli_tool)

 ## Inroduction
 The configuration of the Flipmouse parameters happens
 across Serial Communication at a baudrate of `115200`. 
 The FlipMouse is designed in such a way that the 
 device will understand different *command strings* 
 when sent accross the port. There is a connection 
 sequence that has to be followed so as to successfully 
 connect with the FlipMouse in 
 [Serial Connect Mode](https://gitlab.com/icfoss/Assistive_Tech/flipmouse/tree/master/firmware/Flipmouse#serial-connect-mode).

 The connection sequence is described below 
 *[(a part of documentation taken from the FlipMouse project )](https://gitlab.com/icfoss/Assistive_Tech/flipmouse/tree/master/firmware/Flipmouse#serial-connect-mode)*

 >To connect with the 0FlipMouse, first send '!#!' through the 
 connected serial port. Once connected with the FlipMouse, to 
 send commands, follow the instruction below. 
 ```command format -  *#*_ _ _ _ ```.
The commands are recieved as character string with maximum number 
of 6 characters.Every command should start with *#* and the length 
of the command can be 4 or 6 depending upon the number of arguments 
in the commands. The 3rd and 4th bits conveys the command type and 
5th and 6th bit are bits that pass the value of any particular 
variable in the command.
                .........[read more](https://gitlab.com/icfoss/Assistive_Tech/flipmouse/tree/master/firmware/Flipmouse#serial-connect-mode)

This Cli_tool will help you construct command strings and translate
the simple inputs into messages that can be parsed by the FlipMouse.

## How to run the script
### commandLine.py
> Python3 library Dependencies: **sys** *(part of Python's standard library)*,
 **[serial](https://pypi.org/project/serial/)** ,
 **[glob](https://docs.python.org/3/library/glob.html)** ,
 **time**  *(part of Python's standard library)*,
 **[tabCompletion](https://pypi.org/project/tabCompletion/)** ,
 **os**  *(part of Python's standard library)* ,
 **json**  *(part of Python's standard library)*

Once all the dependencies are installe and you are all set to go, run
the sript with the following command.
```
python3 commandLine.py
```
**Note**: Make sure that the user has permission to access the USB ports
or else follow the steps below to dialout your user for permission.

##### Port Pemissions
```
sudo usermod -a -G dialout $USER
```
where the ``$USER`` is yur username. Logout and login or reboot for 
the effects to take shape.

### commands.json
This file is used for implementing the autcomplete feature in the
Cli_Tool. This file is  fed as the input for the tabCompletion
module, from which the possible commands are recognised by the 
script so as to provide auto completion.

>*It contains all the possible commands in the Cli_Tool.*  


## How to plug in your FlipMouse
In order to configure your FlipMouse, we have to connect in 
``Serial Connect Mode``. For doing so, connect the FlipMouse
to the USB port while pressing the mode button. This will 
make the FlipMouse to switch over to 
[Serial Connet Mode](https://gitlab.com/icfoss/Assistive_Tech/flipmouse/tree/master/firmware/Flipmouse#serial-connect-mode).
Now [run the script](https://gitlab.com/icfoss/Assistive_Tech/flipmouse-configuration-tool_cli#how-to-run-the-script)
and select your port from the list. 

(***Note:*** *If you find no ports
listed please check if the [R/W permissions](https://gitlab.com/icfoss/Assistive_Tech/flipmouse-configuration-tool_cli/#port-pemissions) is given to the port
for your user*)
Once you select your port from the list, cli_tool will run
the connection sequence for establishing healthy connection
between the CLI_Tool and the FlipMouse. Once you have completed
the connection sequence, you are ready for sending your commands 
to your FlipMouse. 

## Connection Status and Firmware Version
Once after the CLI_Tool have completed the connection sequence,
You will get the current version of FlipMouse firmware and at the
same time the CLI_Tool will ask you wether you have to backup the 
current custom parameters from the FlipMouse. 

## Commands in the CLI_Tool
